Mohammad Khan
COMP 313

Project 2

1. Changing to LinkedList results in tests taking longer to complete. Went from .169 s to .206 s
2. list.remove(77) does not work because it tried to remove the element at index 77. This does not work when there are not atleast 77 elements in the collection. 
3. list.remove(5) removes the 5th element in the list while list.remove(Integer.valueOf(5)) removes the element with the value 5 from the list. 
4. 
Test Times(milliseconds):

list size: 10
LinkedListAddRemove: 13    ArrayAddRemove: 26    LinkedListAccess: 16    ArrayListAccess: 44

list size: 100
LinkedListAddRemove: 14    ArrayAddRemove: 33    LinkedListAccess: 36    ArrayListAccess: 51

list size: 1000
LinkedListAddRemove: 13    ArrayAddRemove: 25    LinkedListAccess: 314   ArrayListAccess: 334

list size: 10000
LinkedListAddRemove: 15    ArrayAddRemove: 30    LinkedListAccess: 4621  ArrayListAccess: 2988

As size increases, LinkedList is always fastest at addremove. However, for access ArrayList is much better. 